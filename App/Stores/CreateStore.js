import { applyMiddleware, createStore, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { persistReducer, persistStore } from "redux-persist";
import storage from "@react-native-community/async-storage";

const persistConfig = {
  key: "root",
  storage: storage,
  blacklist: ["startUp"],
};

export default (rootReducer, rootSaga) => {
  console.log({ rootReducer, rootSaga });

  const middleware = [];

  // Connect the sagas to the redux store
  const sagaMiddleware = createSagaMiddleware();
  middleware.push(sagaMiddleware);

  const persistedReducer = persistReducer(persistConfig, rootReducer);

  const store = createStore(
    persistedReducer,
    compose(applyMiddleware(...middleware))
  );
  const persistor = persistStore(store);

  // Kick off the root saga
  sagaMiddleware.run(rootSaga);

  return { store, persistor };
};
